# Set up a Project and Its Dependencies

## Task

Please do the following steps:

1. Create a `package.json` file.
2. Install TypeScript as a dev dependency.
3. Install rxjs library as a main dependency.
4. Install json-server as a global package.
5. Create a `db.json` file and add the following content to it:

    { 
      "tasks": [ 
        { 
          "id": 1, 
          "action": "Estimate", 
          "priority": 3, 
          "estHours": 8 
        }, 
        { 
          "id": 2, 
          "action": "Create", 
          "priority": 2, 
          "estHours": 8 
        }
      ] 
    }

6. Add the following command to the scripts section of the package.json: `"start": "json-server --watch db.json"`.
7. Provide the mentor with:
    - A screenshot of the content of the `package.json` file.
    - A screenshot of the content of a terminal window after executing the command `"npm start"`.
    - A link to the GitLab project with installed dependencies and dev-dependencies.

### How this task will be evaluated

We expect your solution to the task to meet the following criteria:

- You have created a `package.json` file (3 points).
- You have installed TypeScript dependency (3 points).
- You have installed the rxjs library as the main dependency (3 points).
- You have used The GitLab editor (3 points).
- You have created The `db.json` file (3 points).

You can get the maximum of 15 points for this task. To pass the task, you need to get 70% of the points.